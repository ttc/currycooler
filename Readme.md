# Curry Cooler
A Single page app for users to find, reorder and print content.

> This is a work in progress and the process steps will be changed as the project progresses

## Requirements

1. [nodejs](https://nodejs.org/en/)
  2. Recommended: [install npm and nodejs](https://docs.npmjs.com/getting-started/installing-node)

> **Note:** It is also recommended **NOT** to use use the installer packages provided by your operating system package manager, but to
rather use [nvm](https://github.com/creationix/nvm).

## Installation

1. Clone the repository

```Shell
git clone https://gitlab.com/ttc/currycooler.git
cd currycooler
```

2. Install dependencies

```
npm install
```

3. Run in development mode (locally on your computer)

```
npm run start
```

4. Build the project for production (creates the `dist` directory)

```
npm run build
```

## Folder structure

```JavaScript

config //config files - weback and test configs
└── jest
public //static elements
├── index.html // base html file
├── assets // static assets of the project
│   ├── fonts
│   └── images
└── vendor // third party static assets (e.g bootstrap)
    ├── css
    ├── fonts
    └── js
scripts // scripts for running tests, dev and build
src // source files for the project
├── js
│   ├── components
│   └── lib
└── scss
    └── partials
test // test scripts
└── js

```
